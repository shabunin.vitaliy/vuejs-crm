import Vue from 'vue'
import VueRouter from 'vue-router'
import firebase from "firebase/app";

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    meta: {layout: 'auth'},
    component: () => import('../views/Login.vue')
  },
  {
    path: '/register',
    name: 'register',
    meta: {layout: 'auth'},
    component: () => import('../views/Register.vue')
  },
  {
    path: '/categories',
    name: 'categories',
    meta: {layout: 'app', auth: true},
    component: () => import('../views/Categories.vue')
  },
  {
    path: '/history',
    name: 'history',
    meta: {layout: 'app', auth: true},
    component: () => import('../views/History.vue')
  },
  {
    path: '/',
    name: 'home',
    meta: {layout: 'app', auth: true},
    component: () => import('../views/Home.vue')
  },
  {
    path: '/planning',
    name: 'planning',
    meta: {layout: 'app', auth: true},
    component: () => import('../views/Planning.vue')
  },
  {
    path: '/profile',
    name: 'profile',
    meta: {layout: 'app', auth: true},
    component: () => import('../views/Profile.vue')
  },
  {
    path: '/record',
    name: 'record',
    meta: {layout: 'app', auth: true},
    component: () => import('../views/Record.vue')
  },
  {
    path: '/details/:id',
    name: 'details',
    meta: {layout: 'app', auth: true},
    component: () => import('../views/Details.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const requireAuth = to.matched.some(r => r.meta.auth);
  if (!requireAuth) {
    next();
    return;
  }

  const currentUser = firebase.auth().currentUser;
  if (currentUser) {
    next();
    return;
  }

  next('/login?message=login');
});

export default router
