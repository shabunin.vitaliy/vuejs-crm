export default {
    'logout': 'Вы вышли из системы!',
    'login': 'Авторизуйтесь',
    'auth/email-already-in-use': 'Пользователь с таким email уже есть',
    'something_went_wrong': 'Что-то пошло не так',
    'auth/user-not-found': 'Такого пользователя нет',
    'auth/wrong-password': 'Неверный пароль',
    'auth/too-many-requests': 'Много запросов',
    'insufficient_funds': 'Недостаточно средств'
}