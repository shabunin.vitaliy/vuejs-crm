export default {
    install(Vue, options) {
        Vue.prototype.$message = function (text, type) {
            if (!type) {
                type = 'success';
            }

            if ('error' === type) {
                text = '[Ошибка] ' + text;
            }

            M.toast({html: text});
        }
    }
}