import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import 'materialize-css/dist/js/materialize.min';
import dateFormat from '@/filters/date-format';
import localization from '@/filters/localization';
import Vuelidate from "vuelidate/src";
import messagePlugin from '@/utils/notifications';
import firebase from "firebase/app";
import 'firebase/auth';
import 'firebase/database';
import Loader from "@/components/layout/Loader";
import currency from "@/filters/currency";
import tooltip from "@/directives/tooltip";
import Paginate from 'vuejs-paginate';

Vue.config.productionTip = false

Vue.use(messagePlugin);
Vue.use(Vuelidate);
Vue.filter('dateFormat', dateFormat);
Vue.filter('currency', currency);
Vue.filter('localization', localization);
Vue.directive('tooltip', tooltip);
Vue.component('Loader', Loader);
Vue.component('Paginate', Paginate);

firebase.initializeApp({
  apiKey: "AIzaSyAFTCjauASpZiAz8X54fMxsYg4pBKv2pp8",
  authDomain: "vue-crm-test-7ff68.firebaseapp.com",
  projectId: "vue-crm-test-7ff68",
  storageBucket: "vue-crm-test-7ff68.appspot.com",
  messagingSenderId: "567162375319",
  appId: "1:567162375319:web:c827e7aaf7c44dc9702dd2",
  measurementId: "G-3M6Z1BP48T"
});

let app;

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
});



