export default function currency(value, currency = 'UAH') {
    return new Intl.NumberFormat('ua-UA', {
        style: 'currency',
        currency
    }).format(value);
}