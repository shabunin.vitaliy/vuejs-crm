import store from '../store';

export default function dateFormat(value, format = 'date') {

    const locale = store.getters.getUserInfo.locale || 'en-US';

    const options = {};

    if (format.includes('date')) {
        options.day = 'numeric';
        options.month = 'long';
        options.year = 'numeric';
    }

    if (format.includes('time')) {
        options.second = 'numeric';
        options.minute = 'numeric';
        options.hour = 'numeric';
    }

    return new Intl.DateTimeFormat(locale, options).format(value);
}