import firebase from 'firebase/app';

export default {
    actions: {
        async addCategory({commit, dispatch}, {name, limit}) {
            try {
                const id = await dispatch('getId');
                const category = await firebase.database().ref(`/users/${id}/categories`).push({
                    name,
                    limit
                });

                return {name, limit, id: category.key};
            } catch (e) {
                commit('setErrorMessage', e);
                throw e;
            }
        },
        async updateCategory({commit, dispatch}, {id, name, limit}) {
            try {
                const userId = await dispatch('getId');
                await firebase.database().ref(`/users/${userId}/categories/${id}`).set({
                    name, limit
                });
            } catch (e) {
                commit('setErrorMessage', e);
                throw e;
            }
        },
        async getCategories({commit, dispatch}) {
            try {
                const categories = await dispatch('getCategoriesWithKeys');

                return Object.keys(categories).map(key => ({...categories[key], id: key}));
            } catch (e) {
                commit('setErrorMessage', e);
                throw e;
            }
        },
        async getCategoriesWithKeys({commit, dispatch}) {
            try {
                const id = await dispatch('getId');
                let categories = await firebase.database().ref(`/users/${id}/categories`).once('value');
                return categories.val();
            } catch (e) {
                commit('setErrorMessage', e);
                throw e;
            }
        },
        async getCategory({commit, dispatch}, id) {
            try {
                const userId = await dispatch('getId');
                let category = await firebase.database().ref(`/users/${userId}/categories/${id}`).once('value');
                return category.val();
            } catch (e) {
                commit('setErrorMessage', e);
                throw e;
            }
        }
    }
}