import firebase from "firebase/app";

export default {
    state: {
        info: {}
    },
    getters: {
        getUserInfo(state) {
            return state.info;
        }
    },
    mutations: {
        setUserInfo(state, info) {
            state.info = info;
        },
        clearUserInfo(state) {
            state.info = {};
        }
    },
    actions: {
        async getUserInfo({dispatch, commit}) {
            try {
                const id = await dispatch('getId');
                let info = await firebase.database().ref(`/users/${id}/info`).once('value');
                info = info.val();

                commit('setUserInfo', info);
            } catch (e) {
                commit('setErrorMessage', e);
                throw e
            }
        },
        async updateInfo({commit, dispatch, getters}, info) {
            try {
                const userId = await dispatch('getId');

                await firebase.database().ref(`users/${userId}/info`).update(info);

                const userInfo = getters.getUserInfo;

                commit('setUserInfo', {...userInfo, ...info});

            } catch (e) {
                commit('setErrorMessage', e);
                throw e
            }
        }
    }
}