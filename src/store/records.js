import firebase from 'firebase/app';

export default {
    actions: {
        async saveRecord({dispatch, commit, getters}, {categoryId, type, amount, description, date, userId}) {
            try {

                const userId = await dispatch('getId');

                await firebase.database().ref(`users/${userId}/history`).push({
                    categoryId,
                    type,
                    amount,
                    description,
                    date
                });

            } catch (e) {
                commit('setErrorMessage', e);
                throw e
            }
        },
        async getRecords({dispatch, commit}) {
            try {
                const userId = await dispatch('getId');
                const records = (await firebase.database().ref(`/users/${userId}/history`).once('value')).val() || {};

                return Object.keys(records).map(key => ({...records[key], id: key}));
            } catch (e) {
                commit('setErrorMessage', e);
                throw e;
            }
        },
        async getRecord({dispatch, commit}, id) {
            try {
                const userId = await dispatch('getId');
                const records = (await firebase.database().ref(`/users/${userId}/history/${id}`).once('value')).val() || {};

                return records;
            } catch (e) {
                commit('setErrorMessage', e);
                throw e;
            }
        }
    }
}