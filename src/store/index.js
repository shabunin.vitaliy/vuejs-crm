import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import user from "./user";
import categories from "@/store/categories";
import records from "@/store/records";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    errorMessage: null
  },
  mutations: {
    setErrorMessage(state, error) {
      state.errorMessage = error;
    },
    clearErrorMessage(state) {
      state.errorMessage = null
    }
  },
  actions: {
    getCurrencies() {
      return {
        base: 'UAH',
        date: new Date(),
        rates: {
          UAH: 1,
          USD: 27,
          EUR: 30
        }
      };
    }
  },
  getters: {
    getErrorMessage(state) {
      return state.errorMessage;
    }
  },
  modules: {
    auth, user, categories, records
  }
})
