import _ from 'lodash';

export default {
    data() {
        return {
            page: +this.$route.query.page || 1,
            itemsPerPage: 2,
            totalPages: 0,
            allItems: [],
            items: []
        };
    },
    methods: {
        initPaginator(items) {
            this.allItems = _.chunk(items, this.itemsPerPage);
            this.items = this.allItems[this.page - 1] || this.allItems[0];
            this.totalPages = this.allItems.length;
        },
        changePage(page) {
            this.$router.push(`${this.$route.path}?page=${page}`);
            this.items = this.allItems[page - 1] || this.allItems[0];
        }
    }
}